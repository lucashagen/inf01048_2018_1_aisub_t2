import interfaces as controller_template
from itertools import product
from typing import Tuple, List, Any
import math
import random
import re

primes = (2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101,
          103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193)


#F1 defines
NO_BREATHE = 0
NEED_BREATHE = 1

#F2 defines
TOP_PERFECT = 0
TOP_OK = 1
TOP_DANGER = 2
TOP_DEATH = 3

#F3 defines
BOTTOM_PERFECT = 0
BOTTOM_OK = 1
BOTTOM_DANGER = 2
BOTTOM_DEATH = 3

#F4 defines
AHEAD_UP_PERFECT = 0
AHEAD_UP_OK = 1
AHEAD_UP_DANGER = 2
AHEAD_UP_DEATH = 3

#F5 defines
AHEAD_DOWN_PERFECT = 0
AHEAD_DOWN_OK = 1
AHEAD_DOWN_DANGER = 2
AHEAD_DOWN_DEATH = 3

class State(controller_template.State):
    def __init__(self, sensors: list):
        self.sensors = sensors

    def compute_features(self) -> Tuple:
        """
        This function should take the raw sensor information of the submarine (see below) and compute useful features for selecting an action
        The submarine has the following sensors:

        self.sensors contains (in order):

        0 water_UP: 1-700
        1 water_UP_RIGHT: 1-700
        2 obstacle_UP: 1-700
        3 obstacle_UP_RIGHT: 1-700
        4 obstacle_AHEAD: 1-700
        5 obstacle_DOWN_RIGHT: 1-700
        6 obstacle_DOWN: 1-700
        7 monster_UP: 1-200
        8 monster_UP_RIGHT: 1-200
        9 monster_AHEAD: 1-200
        10 monster_DOWN_RIGHT: 1-200
        11 monster_DOWN: 1-200
        12 oxygen: 1-400
          (see the specification file/manual for more details)
        :return: A Tuple containing the features you defined
        """

        water_UP            = self.sensors[0]   # 1-700
        water_UP_RIGHT      = self.sensors[1]   # 1-700
        obstacle_UP         = self.sensors[2]   # 1-700
        obstacle_UP_RIGHT   = self.sensors[3]   # 1-700
        obstacle_AHEAD      = self.sensors[4]   # 1-700
        obstacle_DOWN_RIGHT = self.sensors[5]   # 1-700
        obstacle_DOWN       = self.sensors[6]   # 1-700
        monster_UP          = self.sensors[7]   # 1-200
        monster_UP_RIGHT    = self.sensors[8]   # 1-200
        monster_AHEAD       = self.sensors[9]   # 1-200
        monster_DOWN_RIGHT  = self.sensors[10]  # 1-200
        monster_DOWN        = self.sensors[11]  # 1-200
        oxygen              = self.sensors[12]  # 1-400



        #Will to Breathe# TODO:
        f1 =  water_UP # oxygen * 1.75)
        #Will to Avoid Hitting UPWARDS
        f2 =  min(obstacle_UP, obstacle_UP_RIGHT)
        #Will to Avoid Hitting DOWNWARDS
        f3 =  min(obstacle_DOWN, obstacle_DOWN_RIGHT)
        #Avoiding Ahead obstacles by going UP
        f4 =  min(obstacle_AHEAD, obstacle_UP_RIGHT, obstacle_DOWN_RIGHT)
        #Avoiding Ahead obstacles by going DOWN
        f5 =  min(monster_AHEAD, monster_UP_RIGHT, monster_DOWN_RIGHT)


        return [f1, f2, f3, f4, f5]

    def discretize_features(self, features: Tuple) -> Tuple:
        f1 = 0
        f2 = 0
        f3 = 0
        f5 = 0
        f4 = 0

        #discretizing f1
        if features[0] > 650:
            f1 = NO_BREATHE
        else:
            f1 = NEED_BREATHE

        #discretizing f2
        if features[1] >= 200:
            f2 = TOP_PERFECT
        elif features[1] >= 100 :
            f2 = TOP_OK
        elif features[1] >= 40 :
            f2 = TOP_DANGER
        else:
            f2 = TOP_DEATH

        #discretizing f3
        if features[2] >= 200:
            f3 = BOTTOM_PERFECT
        elif features[2] >= 100 :
            f3 = BOTTOM_OK
        elif features[2] >= 50 :
            f3 = BOTTOM_DANGER
        else:
            f3 = BOTTOM_DEATH

        #discretizing f4
        if features[3] >= 200:
            f4 = AHEAD_UP_PERFECT
        elif features[3] >= 100 :
            f4 = AHEAD_UP_OK
        elif features[3] >= 50 :
            f4 = AHEAD_UP_DANGER
        else:
            f4 = AHEAD_UP_DEATH

        #discretizing f5
        if features[4] >= 200:
            f5 = AHEAD_DOWN_PERFECT
        elif features[4] >= 100 :
            f5 = AHEAD_DOWN_OK
        elif features[4] >= 50 :
            f5 = AHEAD_DOWN_DANGER
        else:
            f5 = AHEAD_DOWN_DEATH

        return [f1, f2, f3, f4, f5]

    def get_discrete_features(self) -> Tuple:
        return self.discretize_features(self.compute_features())

    @staticmethod
    def discretization_levels() -> Tuple:
        """
        This function should return a vector specifying how many discretization levels to use for each state feature.
        :return: A tuple containing the discretization levels of each feature
        """
        return [2,4,4,4,4]

    def get_current_state(self):
        """
        :return: computes the discretized features associated with this state object.
        """
        features = self.discretize_features(self.compute_features())
        return State.get_state_id(features)

    @staticmethod
    def get_state_id(discretized_features: Tuple) -> int:
        """
        Handy function that calculates an unique integer identifier associated with the discretized state passed as
        parameter.
        :param discretized_features
        :return: unique key
        """

        """terms = [primes[i] ** discretized_features[i] for i in range(len(discretized_features))]

        s_id = 1
        for i in range(len(discretized_features)):
            s_id = s_id * terms[i]"""

        levels = State.discretization_levels()

        # Faster
        s_id = 0
        for i in range(len(discretized_features)):
            s_id *= levels[i]
            s_id += discretized_features[i]

        return s_id

    @staticmethod
    def get_number_of_states() -> int:
        """
        Handy function that computes the total number of possible states that exist in the system, according to the
        discretization levels specified by the user.
        :return:
        """
        v = State.discretization_levels()
        num = 1

        for i in (v):
            num *= i

        return num

    @staticmethod
    def enumerate_all_possible_states() -> List:
        """
        Handy function that generates a list with all possible states of the system.
        :return: List with all possible states
        """
        levels = State.discretization_levels()
        levels_possibilities = [(j for j in range(i)) for i in levels]
        return [i for i in product(*levels_possibilities)]

class QTable(controller_template.QTable):
    def __init__(self):
        """
        This class is used to create/load/store your Q-table. To store values we strongly recommend the use of a Python
        dictionary.
        """
        self.states = {}
        statesList = State.enumerate_all_possible_states()

        for i in range (len(statesList)):
            self.states[State.get_state_id(statesList[i])] = [0.0, 0.0] # [JUMP, DO_NOTHING]

    def get_q_value(self, key: State, action: int) -> float:
        """
        Used to securely access the values within this q-table
        :param key: a State object
        :param action: an action
        :return: The Q-value associated with the given state/action pair
        """
        return self.states[key.get_current_state()][action - 1]

    def get_max_q_value(self, key: State) -> float:
        """
        Used to securely access the max value within this q-table
        :param key: a State object
        :return: The max Q-value associated with the given state
        """
        return max(self.states[key.get_current_state()])

    def set_q_value(self, key: State, action: int, new_q_value: float) -> None:
        """
        Used to securely set the values within this q-table
        :param key: a State object
        :param action: an action
        :param new_q_value: the new Q-value to associate with the specified state/action pair
        :return:
        """
        self.states[key.get_current_state()][action-1] = new_q_value

    def save(self, path: str, *args) -> None:
        """
        This method must save this QTable to disk in the file file specified by 'path'
        :param path:
        :param args: Any optional args you may find relevant; beware that they are optional and the function must work
                     properly without them.
        """
        file = open(path, "w")

        data = ""
        for k,v in self.states.items():
            data += "[" + str(k) + "|" + str(v[0]) + ":" + str(v[1]) + "]\n"
            # print(v)

        file.write(data)
        file.close()

    def printValues(self):
        for k,v in self.states.items():
            print(k, "=", v)

    @staticmethod
    def load(path: str) -> "QTable":
        """
        This method should load a Q-table from the specified file and return a corresponding QTable object
        :param path: path to file
        :return: a QTable object

            0: id
            1: jump point
            2: no-jump point

        """
        file = open(path, "r")
        strIn = file.read()
        file.close()

        regex = re.compile("\\[(\\d+)[|](-?\\d+(?:[.]\\d+)?):(-?\\d+(?:[.]\\d+)?)\\]")
        data = regex.findall(strIn)

        if len(data) == 0:
            raise Exception("WRONG FORMAT")

        table = QTable()
        for line in data:
            table.states[int(line[0])] = [float(line[1]), float(line[2])]

        # table.printValues()
        return table




class Controller(controller_template.Controller):
    def __init__(self, q_table_path: str):
        random.seed()
        if q_table_path is None:
            self.q_table = QTable()
        else:
            self.q_table = QTable.load(q_table_path)

    def compute_reward(self, new_state: State, old_state: State, action: int, n_steps: int,
                       end_of_episode: bool) -> float:
        """
        This method is called by the learn() method in simulator.Simulation() to calculate the reward to be given to the agent
        :param new_state: The state the sub just entered
        :param old_state: The state the sub just left
        :param action: the action the sub performed to get in new_state
        :param n_steps: number of steps the sub has taken so far in the current race
        :param end_of_episode: boolean indicating if an episode has ended
        :return: The reward to be given to the agent
        """
        # Sistema de rewards usado para a competicao. Foi o que conseguiu 32k uma hora e chegou mais longe
        #para o relatori/apresentacao comentaremos algumas outras abordagens que testamos e os aspectos positivos e negativos delas

        if end_of_episode:
            return -100.0
        else:
            if new_state.sensors[12] > old_state.sensors[12]:
                # print("WATER")
                return 100000.0
            else:
                return 0.0
            #return 10 + (new_state.sensors[12] - old_state.sensors[12]) / 100.0

    def update_q(self, new_state: State, old_state: State, action: int, reward: float, end_of_race: bool) -> None:
        """
        This method is called by the learn() method in simulator.Simulation() to update your Q-table after each action is taken
        :param new_state: The state the sub just entered
        :param old_state: The state the sub just left
        :param action: the action the sub performed to get to new_state
        :param reward: the reward the sub received for getting to new_state
        :param end_of_episode: boolean indicating if an episode has ended
        """
        alpha = 0.5
        gama = 1

        # if reward < 0 or self.q_table.get_max_q_value(new_state) < 0:
        #     newPoints = -1
        # else:
        newPoints = (1 - alpha) * self.q_table.get_q_value(old_state, action) + alpha * (reward + gama * self.q_table.get_max_q_value(new_state))
        self.q_table.set_q_value(old_state, action, newPoints)

    def take_action(self, new_state: State, episode_number: int) -> int:
        """
        Decides which action the submarine must execute based on its Q-Table and on its exploration policy
        :param new_state: The current state of the submarine
        :param episode_number: current episode/race during the training period
        :return: The action the submarine chooses to execute
        """
        E = -episode_number/100 + 0.5 # 5% das vezes -> ação aleatória
        JUMP = self.q_table.get_q_value(new_state, 1)
        DO_NOTHING = self.q_table.get_q_value(new_state, 2)

        if(JUMP < 0):
            return 2
        if(DO_NOTHING < 0):
            return 1

        if(random.uniform(0, 1) < E):        # RANDOM ACTION
            return random.randint(1,2)

        if(JUMP > DO_NOTHING):
            return 1
        else:
            return 2
